#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente que abre un socket a un servidor
"""

import socket
import sys

try:
    METODO = sys.argv[1]
    RECEPTOR = sys.argv[2].split(':')[0]
    DIRECCION = RECEPTOR.split('@')[1]
    PORT = int(sys.argv[2].split(':')[1])
except (IndexError, ValueError):
    sys.exit("Usage: python3 client.py method receiver@IP:SIPport")


# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
try:
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        my_socket.connect((DIRECCION, PORT))

        if METODO == 'INVITE' or METODO == 'BYE':
            print("Enviando:", METODO + ' sip:' + RECEPTOR + ' SIP/2.0\r\n\r\n')
            my_socket.send(bytes(METODO + ' sip:' + RECEPTOR + ' SIP/2.0\r\n\r\n', 'utf-8') + b'\r\n\r\n')
            data = my_socket.recv(1024)
            print('Recibido -- ', data.decode('utf-8'))
            mensaje = data.decode('utf-8').split(' ')
            for element in mensaje:
                if METODO != 'BYE' and element == '200':
                    my_socket.send(bytes('ACK sip:' + RECEPTOR.split(':')[0] +' SIP/2.0\r\n', 'utf-8') + b'\r\n')
            print("Terminando socket...")
        
        print("Fin.")

except ConnectionRefusedError:
    print("Error de conexión")
